/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import controller.Utils;
import org.hibernate.Session;

/**
 *
 * @author necio
 */
public class Dao <T> {
    
    private Session session;
    
    public Dao(){
		session = Utils.getSession();
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }        
    
    public void save(T t){        
        //try{
            session = Utils.getSession();
            session.beginTransaction();
            session.save(t);
            session.getTransaction().commit();
            session.close();
        //} catch (SQLException sql){
            //JOptionPane.showMessageDialog(null, sql.getMessage(), "Erro na gravação", JOptionPane.ERROR_MESSAGE);
        //}

    }
    
    public void atualizar(T t){
        session = Utils.getSession();
	session.beginTransaction();
	session.saveOrUpdate(t);
	session.getTransaction().commit();
        session.close();
    }
	
    public void excluir(T t){
        session = Utils.getSession();
	session.beginTransaction();
	session.delete(t);
	session.getTransaction().commit();
        session.close();
    }
	
    public void close(){
	if (session.isOpen())
            session.close();
    }
    
}
