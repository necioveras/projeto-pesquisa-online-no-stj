/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import com.sun.glass.events.KeyEvent;
import controller.ProcessAnalyzedControler;
import controller.QueryProcessControler;
import java.awt.Color;
import java.awt.Component;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import model.Query;
import model.STJProcess;

/**
 *
 * @author necio
 */
public class RecoveredQueries extends javax.swing.JFrame {

    /**
     * Creates new form RecoveredQueries
     */
    public RecoveredQueries() {
        initComponents();
        loadQueries();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        jPanel3 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new javax.swing.BoxLayout(getContentPane(), javax.swing.BoxLayout.PAGE_AXIS));

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Pesquisas realizadas"));

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null}
            },
            new String [] {
                "Código", "Data", "Rótulo", "Texto da pesquisa"
            }
        ));
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jTable1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTable1KeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 757, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        getContentPane().add(jPanel2);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Dados recuperados"));
        jPanel1.setLayout(new java.awt.CardLayout());

        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null}
            },
            new String [] {
                "Id", "Processo", "Relator", "Data do julgamento", "Turma", "Analisado"
            }
        ));
        jTable2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable2MouseClicked(evt);
            }
        });
        jTable2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTable2KeyPressed(evt);
            }
        });
        jScrollPane2.setViewportView(jTable2);

        jPanel1.add(jScrollPane2, "card2");

        getContentPane().add(jPanel1);

        jPanel3.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.RIGHT));

        jLabel1.setText("Total de processos: ");
        jPanel3.add(jLabel1);
        jPanel3.add(jLabel2);

        getContentPane().add(jPanel3);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        loadProcess();
    }//GEN-LAST:event_jTable1MouseClicked

    private void jTable1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable1KeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER){
            loadProcess();        
            evt.consume();
        }
    }//GEN-LAST:event_jTable1KeyPressed

    private void jTable2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable2MouseClicked
        // TODO add your handling code here:
        toAnalyze();
    }//GEN-LAST:event_jTable2MouseClicked

    private void jTable2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable2KeyPressed
        // TODO add your handling code here:
        if (evt.getKeyCode() == KeyEvent.VK_ENTER)
            toAnalyze();
    }//GEN-LAST:event_jTable2KeyPressed

    private void toAnalyze(){
        if (jTable2.getSelectedRow() != -1){
            int processId = Integer.parseInt(jTable2.getValueAt(jTable2.getSelectedRow(), 0).toString());        
            Analyze analyze = new Analyze(processId);
            analyze.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            analyze.pack();
            analyze.setLocationRelativeTo(null);
            analyze.setVisible(true);
        } else {
            JOptionPane.showMessageDialog(null, "Selecione um processo na tabela para realizar a análise", "Seleção de processo", JOptionPane.ERROR_MESSAGE);
        }               
    }
    
    private void loadProcess(){
                
        int queryId = Integer.parseInt(jTable1.getValueAt(jTable1.getSelectedRow(), 0).toString());
        
        List<STJProcess> list = QueryProcessControler.loadProcess(queryId);
        List<Integer> idsAnalyzed = ProcessAnalyzedControler.getIDsSTJProcess();
        
        DefaultTableModel model = (DefaultTableModel) jTable2.getModel();       
        model.setRowCount(1);
        
        for (STJProcess stjProcess : list){
            int row = jTable2.getRowCount()-1;
            jTable2.getModel().setValueAt(stjProcess.getId(), row, 0);
            jTable2.getModel().setValueAt(stjProcess.getNumber(), row, 1);
            jTable2.getModel().setValueAt(stjProcess.getMinister(), row, 2);
            jTable2.getModel().setValueAt(stjProcess.getJudgeDate(), row, 3);
            jTable2.getModel().setValueAt(stjProcess.getTeam(), row, 4);
            
            if (idsAnalyzed.contains(stjProcess.getId()))
                jTable2.getModel().setValueAt("Sim", row, 5);
            else
                jTable2.getModel().setValueAt("Não", row, 5);
            
            checkAnalyzed();
            
            model.addRow(new Object[ jTable2.getColumnCount() ]);
            
        }
        jLabel2.setText(Integer.toString(jTable2.getRowCount()-1));
    }
    
    private void checkAnalyzed(){
        jTable2.setDefaultRenderer(Object.class, new DefaultTableCellRenderer() {
            @Override
            public Component getTableCellRendererComponent(JTable table, Object value,
                    boolean isSelected, boolean hasFocus, int row, int column) {
                super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);                                                                                                
                
                Object ref = table.getValueAt(row, 5);
                
                if (ref != null && ref.equals("Sim")) 
                    setForeground(Color.BLUE);
                else 
                    setForeground(Color.BLACK);
                return this;
            }
        });
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(RecoveredQueries.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(RecoveredQueries.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(RecoveredQueries.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(RecoveredQueries.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new RecoveredQueries().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jTable1;
    private javax.swing.JTable jTable2;
    // End of variables declaration//GEN-END:variables

    private void loadQueries() {        
        List<Query> list = QueryProcessControler.loadQueries();
        
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        
        for (Query q : list){
            int row = jTable1.getRowCount()-1;
            jTable1.getModel().setValueAt(q.getId(), row, 0);
            jTable1.getModel().setValueAt(q.getDate(), row, 1);
            jTable1.getModel().setValueAt(q.getLabel(), row, 2);
            jTable1.getModel().setValueAt(q.getText(), row, 3);
            
            model.addRow(new Object[ jTable1.getColumnCount() ]);
            
        }
    }
}
