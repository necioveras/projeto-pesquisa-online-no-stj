/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import javax.swing.JFrame;
import view.DataSTJ;

/**
 *
 * @author necio
 */
public class DataSTJControler {
    
    private DataSTJ dataSTJ;
    
    public DataSTJControler(StjServices service){
        dataSTJ = new DataSTJ(service);
    }
    
    public void startView(String textQuery, boolean acordaos, 
            boolean sumulas, boolean decisoesMonocraticas,
            boolean informativos){        
        dataSTJ.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        dataSTJ.pack();
        dataSTJ.setLocationRelativeTo(null);                
        dataSTJ.setBasicData(textQuery, acordaos, sumulas, decisoesMonocraticas, informativos);
        dataSTJ.setVisible(true);        
        dataSTJ.getData();
    }
    
}
