/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.Dao;
import java.util.List;
import model.Query;
import model.STJProcess;

/**
 *
 * @author necio
 */
public class QueryProcessControler {
    
    
    public static List<Query> loadQueries(){
        Dao<Query> dao = new Dao();
        List<Query> list = dao.getSession().getNamedQuery("getAllQueries").list();
        return list;
    }
    
    public static List<STJProcess> loadProcess(int queryId){
        Dao<Query> dao = new Dao();        
        
        List<STJProcess> list = dao.getSession().getNamedQuery("getProcessByQueryId").
                setInteger(0, queryId).list();
        
        return list;
    }
       
    
}
