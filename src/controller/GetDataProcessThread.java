/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import model.STJProcess;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 *
 * @author necio
 */
public class GetDataProcessThread extends Thread {

    private static int DELAY = 50;

    private JProgressBar progressBar;
    private JButton button;
    private JTable dataTable;
    private StjServices stjServicees;    
    private List<STJProcess> listOfProcess;

    public GetDataProcessThread(JProgressBar bar, JButton buttonActivation, JTable data,
            StjServices stjServices, List<STJProcess> listOfProcess) {
        progressBar = bar;
        this.button = buttonActivation;
        dataTable = data;
        this.stjServicees = stjServices;
        this.listOfProcess = listOfProcess;
    }

    public JProgressBar getProgressBar() {
        return progressBar;
    }

    public void setProgressBar(JProgressBar progressBar) {
        this.progressBar = progressBar;
    }

    public JButton getButton() {
        return button;
    }

    public void setButton(JButton button) {
        this.button = button;
    }

    public JTable getDataTable() {
        return dataTable;
    }

    public void setDataTable(JTable dataTable) {
        this.dataTable = dataTable;
    }

    public StjServices getStjServicees() {
        return stjServicees;
    }

    public void setStjServicees(StjServices stjServicees) {
        this.stjServicees = stjServicees;
    }    

    public List<STJProcess> getListOfProcess() {
        return listOfProcess;
    }

    public void setListOfProcess(List<STJProcess> listOfProcess) {
        this.listOfProcess = listOfProcess;
    }        

    //efetivamente recupera os dados do STJ
    public void run() {
        int minimum = 0;
        int maximum = stjServicees.getTotalDocsFound();

        if (progressBar != null) {
            progressBar.setMaximum(minimum);
            progressBar.setMaximum(maximum);
            progressBar.setString("Aguarde, carregando dados...");
            progressBar.setStringPainted(true);
        }

        if (button != null) {
            button.setEnabled(false);
        }

        if (dataTable != null) {            
            dataTable.removeAll();
        }

        int value = 0, count = 0;
        
        if (listOfProcess != null)
            listOfProcess.clear();
        else        
            listOfProcess = new LinkedList<>();
        
        Document document;
        List<String> resultLinks = stjServicees.getResultsLinks();
        String link = "";
        String lastUrl = stjServicees.getHostUrl() + stjServicees.getPathPost();

        //recupera TODOS os resultados encontrados
        for (String url : resultLinks) {            

            try {

                document = Jsoup.connect(stjServicees.getHostUrl() + url)                        
                        .userAgent("Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:40.0) Gecko/20100101 Firefox/40.0")
                        .referrer(lastUrl)
                        .cookie("opAjuda", "Sim")
                        .cookie("tipo_visualizacao", "null")
                        .cookie("thesaurus", "null")
                        .cookie("operador", "e")
                        .cookie("FGTServer", "A976685F4656113A37EFDE00748A217CAB8E5D9FC1534E96D07846EF3958017472F955")
                        .cookie("base", "ACOR")                        
                        .timeout(20000)   //20s
                        .get();                
                lastUrl = stjServicees.getHostUrl() + url;
              
                //Total encontrado no link ATUAL                
                String infoPesquisa = document.getElementById("infopesquisa").child(1).text();
                if (infoPesquisa != null && progressBar != null){
                    maximum = Integer.parseInt(infoPesquisa);
                    progressBar.setMaximum(maximum);
                }
                                                              
                do {
                    Elements heads = document.getElementsByAttributeValue("id", "acoesdocumento");

                    for (int i = 0; i < heads.size(); i++) {
                        
                        //Não aceita outra TAG que não seja uma DIV
                        if (!heads.get(i).tagName().equals("div"))
                            continue;
                        
                        //Para cada documento da página atual
                        STJProcess process = new STJProcess();
                        Elements elems = heads.get(i).siblingElements();
                        for (Element e : elems){   //buscar cada campo de interesse (por documento)
                            if (e.className().equals("paragrafoBRS")){
                                String title = e.getElementsByClass("docTitulo").first().text();
                                String text  = e.getElementsByClass("docTexto").first().text();
                                switch (title){
                                    case "Processo": 
                                        process.setNumber(text); break;                                       
                                    case "Relator(a)":
                                        process.setMinister(text); break;                                       
                                    case "Relator(a) p/ Acórdão":
                                        process.setMinisterForJudgment(text); break;                                       
                                    case "Órgão Julgador":
                                        process.setTeam(text); break;                                       
                                    case "Data do Julgamento":
                                        process.setJudgeDate(text); break;                                       
                                    case "Data da Publicação/Fonte":
                                        process.setPublishData(text); break;                                       
                                    case "Ementa":
                                        process.setResume(text); break;                                       
                                    case "Acórdão":
                                        process.setJudgment(text); break;
                                    case "Resumo Estruturado":
                                        process.setStructuredResume(text); break;                                        
                                    case "Referência Legislativa":
                                        process.setLegislativeReference(text); break;
                                    case "Doutrina":
                                        process.setDoctrine(text); break;
                                    case "Veja":
                                        process.setToLook(text); break;
                                    case "Notas":
                                        process.setNotes(text); break;
                                    case "Indexação":
                                        process.setIndexing(text); break;
                                    case "Sucessivos":
                                        process.setSucessive(text); break;
                                    case "Outras Informações":
                                    case "Informações Adicionais":
                                        process.setMoreInformations(text); break;
                                }
                            }
                        }
                        
                        /*process.setNumber(elems.get(2).getElementsByClass("docTexto").first().text());
                        process.setMinister(elems.get(3).getElementsByClass("docTexto").first().text());
                        process.setTeam(elems.get(4).getElementsByClass("docTexto").first().text());
                        process.setJudgeDate(elems.get(5).getElementsByClass("docTexto").first().text());
                        process.setPublishData(elems.get(6).getElementsByClass("docTexto").first().text());
                        process.setResume(elems.get(7).getElementsByClass("docTexto").first().text());*/

                        Element linkFullProcess = heads.get(i).child(0).select("a").first();
                        if (linkFullProcess != null) {
                            process.setLinkToFullProcess(linkFullProcess.attr("href"));
                        }

                        listOfProcess.add(process);

                        count++;
                        if (progressBar != null) {
                            progressBar.setString("Aguarde, carregando os dados dos processos (" + count + " de " + maximum + ")");
                            value = progressBar.getValue();
                            progressBar.setValue(value + 1);
                        }
                        if (dataTable != null) {
                            updateDataTable(process);
                        }

                        //Thread.sleep(100);                                                

                    }
                    
                    //Atualizar o documento com a próxima página de resultados                    
                    Element nextPage = document.getElementsByClass("iconeProximaPagina").first();
                    if (nextPage != null){
                        link = nextPage.attr("href");                    
                        document = Jsoup.connect(stjServicees.getHostUrl() + link)
                        .userAgent("Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:40.0) Gecko/20100101 Firefox/40.0")
                        .referrer(lastUrl)
                        .cookie("opAjuda", "Sim")
                        .cookie("tipo_visualizacao", "null")
                        .cookie("thesaurus", "null")
                        .cookie("operador", "e")
                        .cookie("FGTServer", "A976685F4656113A37EFDE00748A217CAB8E5D9FC1534E96D07846EF3958017472F955")
                        .cookie("base", "ACOR")                                                                
                        .timeout(20000)   //20s
                        .get();
                    
                    lastUrl = stjServicees.getHostUrl() + link;
                    }
                    else
                        link = null;                               

                } while (link != null || count < maximum);

            } catch (IOException e) {
                JOptionPane.showMessageDialog(null, "Problemas com a recuperação dos dados. Verifique a establidade da sua conexão com a internet.", 
                        e.getMessage(), JOptionPane.ERROR_MESSAGE);                
                return;
            }
            //} catch (InterruptedException e) {      }
        }

        if (button != null) {
            button.setEnabled(true);
        }
        JOptionPane.showMessageDialog(null, "Dados carregados com sucesso!", "Importaçao de dados", JOptionPane.INFORMATION_MESSAGE);
    }

    private void updateDataTable(STJProcess process) {
        DefaultTableModel model = (DefaultTableModel) dataTable.getModel();
        int colums = dataTable.getColumnModel().getColumnCount();
        model.addRow(new Object[colums]);
        dataTable.setValueAt(process.getNumber(), dataTable.getRowCount() - 1, 0);
        dataTable.setValueAt(process.getMinister(), dataTable.getRowCount() - 1, 1);
        dataTable.setValueAt(process.getTeam(), dataTable.getRowCount() - 1, 2);
        dataTable.setValueAt(process.getJudgeDate(), dataTable.getRowCount() - 1, 3);
        dataTable.setValueAt(process.getPublishData(), dataTable.getRowCount() - 1, 4);
    }

}
