/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.Dao;
import model.STJProcess;

/**
 *
 * @author necio
 */
public class ProcessControler {
    
    public static STJProcess getProcess (int id){
        Dao<STJProcess> dao = new Dao<>();
        return (STJProcess) dao.getSession().getNamedQuery("getProcessById")
                .setInteger(0, id).uniqueResult();
    }
    
}
