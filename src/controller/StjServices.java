/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import javax.swing.JLabel;
import model.STJProcess;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

/**
 *
 * @author necio
 */
public class StjServices {
    
    private JLabel labelStatus;
    private Document document;
    private String hostUrl = "http://www.stj.jus.br";
    private String path = "/SCON/";
    private String pathPost = "/SCON/pesquisar.jsp";
    
    private int totalDocsFound;
    private List<String> resultsLinks; 
    private List<STJProcess> processList;
    
    public StjServices(JLabel labelStatus){
        this.labelStatus = labelStatus;
        totalDocsFound = 0;
        resultsLinks = new LinkedList<>();
    }

    public void connect() throws IOException {                
        
        if (labelStatus != null)
            labelStatus.setText("conectando....");
        
        document = Jsoup.connect(hostUrl + path).get();        
        
        if (labelStatus != null)
            labelStatus.setText("conectado com " + hostUrl);
               
    }
    
    public String pesquisar(String queryText, boolean acordaos, boolean sumulas, 
            boolean decisoesMonocraticas, boolean informativos, boolean todos) throws IOException{        
        
        Connection con = Jsoup.connect(hostUrl + pathPost)                
                .data("acao", "pesquisar")
                .data("novaConsulta", "true")
                .data("i", "1")
                .data("operador", "adj")
                .data("data", "")                
                .data("livre", queryText)
                .data("pesquisaLivre", queryText)
                .data("refinar", "")
                .data("opAjuda", "SIM")
                .data("tipo_visualizacao_doc_toc", "NULL")
                .data("thesaurus", "JURIDICO")
                .data("p", "true")
                .data("conectivo_padrao", "adj")
                .data("processo", "")
                .data("livreMinistro","")
                .data("relator","")
                .data("data_inicial","")
                .data("data_final","")
                .data("tipo_data","DTDE")
                .data("livreOrgaoJulgador","")
                .data("orgao","")
                .data("ementa","")
                .data("ref","")
                .data("siglajud","")
                .data("numero_leg","")
                .data("tipo1","")
                .data("numero_art1","")
                .data("tipo2","")
                .data("numero_art2","")
                .data("tipo3","")
                .data("numero_art3","")
                .data("nota","");                
        
        if (acordaos)
            con.data("b","ACOR");
        if (sumulas)
            con.data("b", "SUM");
        if (decisoesMonocraticas)
            con.data("b", "DTXT");
        if (informativos)
            con.data("b", "INFJ");
        if (todos)
            con.data("b", "todas");
        
        document = con
                .referrer(hostUrl + path)
                .userAgent("Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:40.0) Gecko/20100101 Firefox/40.0")                
                .postDataCharset("ISO-8859-1")
                .timeout(20000)  //20s
                .post();

        StringBuffer sb = new StringBuffer("");
        resultsLinks.clear();
        
        totalDocsFound = 0;
        
        for (Element e : document.getElementsByAttributeValue("id", "itemlistaresultados")){
                sb.append(e.child(0).html() + " - " + e.child(1).text() + "\n");                                            
                Element link = e.child(1).select("a").first();
                if (link != null){
                    resultsLinks.add(link.attr("href"));
                    totalDocsFound += Integer.parseInt(e.child(1).text().split(" ")[0]);
                }
        }
                        
        return sb.toString();
    }

    public int getTotalDocsFound() {
        return totalDocsFound;
    }

    public List<String> getResultsLinks() {
        return resultsLinks;
    }        

    public String getHostUrl() {
        return hostUrl;
    }

    public String getPathPost() {
        return pathPost;
    }
        
        
   
}
