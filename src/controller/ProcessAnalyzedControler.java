/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.Dao;
import java.util.List;
import model.ProcessAnalyzed;

/**
 *
 * @author necio
 */
public class ProcessAnalyzedControler {
    
    public static ProcessAnalyzed getProcessAnalyzed(int processId){
        Dao<ProcessAnalyzed> dao = new Dao<>();
        return (ProcessAnalyzed) dao.getSession().getNamedQuery("findAnalysisByProcessId").
                setInteger(0, processId).uniqueResult();
    }
    
    public static List<Integer> getIDsSTJProcess(){
        Dao<ProcessAnalyzed> dao = new Dao<>();
        return (List<Integer>) dao.getSession().getNamedQuery("recAllsTJProcess").list();
    }
    
    public static long getNumberOfProcessAnalyzed(int queryId){
        Dao<ProcessAnalyzed> dao = new Dao<>();
        return (Long) dao.getSession().getNamedQuery("numberOfProcessAnalyzed")
                .setInteger(0, queryId).uniqueResult();
    }
    
    public static List valuePerEvent(int queryId){
        Dao<ProcessAnalyzed> dao = new Dao<>();
        return dao.getSession().getNamedQuery("valuePerEvent").setInteger(0, queryId).list();
    }
    
    public static List<ProcessAnalyzed> processAnalyzedByEvents(int queryId){
        Dao<ProcessAnalyzed> dao = new Dao<>();
        return dao.getSession().getNamedQuery("processAnalyzedByEvents").setInteger(0, queryId).list();
    }
    
    public static List<ProcessAnalyzed> paramPerEvent(int queryId, String event){
        Dao<ProcessAnalyzed> dao = new Dao<>();
        return dao.getSession().getNamedQuery("paramPerEvent")
                .setInteger(0, queryId)
                .setString(1, event)
                .list();
    } 
    
    public static List<ProcessAnalyzed> doctrinePerEvent(int queryId, String event){
        Dao<ProcessAnalyzed> dao = new Dao<>();
        return dao.getSession().getNamedQuery("doctrinePerEvent")
                .setInteger(0, queryId)
                .setString(1, event)
                .list();
    } 
    
    public static List<ProcessAnalyzed> peculiaritiesPerEvent(int queryId, String event){
        Dao<ProcessAnalyzed> dao = new Dao<>();
        return dao.getSession().getNamedQuery("peculiaritiesPerEvent")
                .setInteger(0, queryId)
                .setString(1, event)
                .list();
    } 
    
    public static List<ProcessAnalyzed> precedntsPerEvent(int queryId, String event){
        Dao<ProcessAnalyzed> dao = new Dao<>();
        return dao.getSession().getNamedQuery("precedntsPerEvent")
                .setInteger(0, queryId)
                .setString(1, event)
                .list();
    } 
    
    public static List<ProcessAnalyzed> valuationPerEvent(int queryId, String event){
        Dao<ProcessAnalyzed> dao = new Dao<>();
        return dao.getSession().getNamedQuery("valuationPerEvent")
                .setInteger(0, queryId)
                .setString(1, event)
                .list();
    }            
    
        
}
