/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.tool.hbm2ddl.SchemaUpdate;

/**
 *
 * @author necio
 */
public class Utils {
    
    private static Configuration configuration = new Configuration();
    private static SessionFactory session; 
    
    public static void configure(){
        configuration.addAnnotatedClass(model.STJProcess.class);
        configuration.addAnnotatedClass(model.Query.class);
        configuration.addAnnotatedClass(model.ProcessAnalyzed.class);
        configuration.addAnnotatedClass(model.ParamAndCriteria.class);
        configuration.addAnnotatedClass(model.Doctrine.class);
        configuration.addAnnotatedClass(model.PeculiaritiesOfConcreteCase.class);
        configuration.addAnnotatedClass(model.Precedents.class);
        configuration.addAnnotatedClass(model.ValuationForConcreteCase.class);
        
        SchemaUpdate schema = new SchemaUpdate(configuration);        
        schema.execute(true, true);
        
        //SchemaExport schema = new SchemaExport(configuration);
        //schema.create(true, true);
        
        
        ServiceRegistry service = new StandardServiceRegistryBuilder().build();
        
        session = configuration.buildSessionFactory(service);
    }
    
    public static Session getSession(){
        return session.openSession();
    }
    
    public static void close(){
        session.close();
    }
    
}
