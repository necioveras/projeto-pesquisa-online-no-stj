/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

/**
 *
 * @author necio
 */
@NamedQueries ({
    @NamedQuery (name = "getAllProcess", query = "from STJProcess"),
    @NamedQuery (name = "getProcessById", query = "from STJProcess where id = ?")    
})
@Entity
public class STJProcess {
    
    @Id 
    @GeneratedValue (strategy = GenerationType.AUTO)
    private int id;
    
    //Automatic fields
    @Column (columnDefinition = "text")
    private String number;          //Numero do processo
    private String minister;        //Ministro 
    private String ministerForJudgment;   // Ministro para acórdão
    private String team;           //Turma
    private String judgeDate;   //Data do julgado
    private String publishData;    //Dados da publicaçao: fonte + data
    @Column (columnDefinition = "text")
    private String resume;         //Ementa
    private String linkToFullProcess;
    @Column (columnDefinition = "text")
    private String judgment; 
    @Column (columnDefinition = "text")
    private String structuredResume;
    @Column (columnDefinition = "text")
    private String legislativeReference;
    @Column (columnDefinition = "text")
    private String doctrine;
    @Column (columnDefinition = "text")
    private String toLook; 
    @Column (columnDefinition = "text")
    private String notes;
    @Column (columnDefinition = "text")
    private String indexing;
    @Column (columnDefinition = "text")
    private String sucessive;
    @Column (columnDefinition = "text")
    private String moreInformations;    
    
    @ManyToOne
    @JoinColumn (name = "query_id")
    private Query query;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Query getQuery() {
        return query;
    }

    public void setQuery(Query query) {
        this.query = query;
    }     
    
    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getMinister() {
        return minister;
    }

    public void setMinister(String minister) {
        this.minister = minister;
    }

    public String getMinisterForJudgment() {
        return ministerForJudgment;
    }

    public void setMinisterForJudgment(String ministerForJudgment) {
        this.ministerForJudgment = ministerForJudgment;
    }
    
    public String getTeam() {
        return team;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public String getJudgeDate() {
        return judgeDate;
    }

    public void setJudgeDate(String judgeDate) {
        this.judgeDate = judgeDate;
    }

    public String getPublishData() {
        return publishData;
    }

    public void setPublishData(String publishData) {
        this.publishData = publishData;
    }

    public String getResume() {
        return resume;
    }

    public void setResume(String resume) {
        this.resume = resume;
    }

    public String getLinkToFullProcess() {
        return linkToFullProcess;
    }

    public void setLinkToFullProcess(String linkToFullProcess) {
        this.linkToFullProcess = linkToFullProcess;
    }

    public String getJudgment() {
        return judgment;
    }

    public void setJudgment(String judgment) {
        this.judgment = judgment;
    }

    public String getStructuredResume() {
        return structuredResume;
    }

    public void setStructuredResume(String structuredResume) {
        this.structuredResume = structuredResume;
    }

    public String getLegislativeReference() {
        return legislativeReference;
    }

    public void setLegislativeReference(String legislativeReference) {
        this.legislativeReference = legislativeReference;
    }

    public String getDoctrine() {
        return doctrine;
    }

    public void setDoctrine(String doctrine) {
        this.doctrine = doctrine;
    }

    public String getToLook() {
        return toLook;
    }

    public void setToLook(String toLook) {
        this.toLook = toLook;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getIndexing() {
        return indexing;
    }

    public void setIndexing(String indexing) {
        this.indexing = indexing;
    }

    public String getSucessive() {
        return sucessive;
    }

    public void setSucessive(String sucessive) {
        this.sucessive = sucessive;
    }

    public String getMoreInformations() {
        return moreInformations;
    }

    public void setMoreInformations(String moreInformations) {
        this.moreInformations = moreInformations;
    }
    
    
    
    public String toString(){
        return "Dados: \n" +
                "Número do preocesso: " + number +
                "Relator: " + minister +
                "Turma: " + team +
                "Data do julgado: " + judgeDate +
                "Dados da publicação: " + publishData +
                "Ementa: " + resume +
                "Link para o processo na íntegra: " + linkToFullProcess;
    }
    
    
    public Object[] toObject(){
        String[] data = new String[5];
        data[0] = number;
        data[1] = minister;
        data[2] = team;
        data[3] = judgeDate;
        data[4] = publishData;
        return data;
    }
    
    
           
}
