/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author necio
 */
@Entity
public class ValuationForConcreteCase{
    
    @Id
    @GeneratedValue (strategy = GenerationType.AUTO)
    private int id;
    
    private String text;
    
    @ManyToOne
    @JoinColumn (name = "processAnalyzed_id")
    private ProcessAnalyzed processAnalyzed;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ProcessAnalyzed getProcessAnalyzed() {
        return processAnalyzed;
    }

    public void setProcessAnalyzed(ProcessAnalyzed processAnalyzed) {
        this.processAnalyzed = processAnalyzed;
    }
    
    
    
    public ValuationForConcreteCase(){
        
    }
    
    public ValuationForConcreteCase(String s){
        text = s;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
    
}
