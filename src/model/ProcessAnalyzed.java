/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

/**
 *
 * @author necio
 */
@NamedQueries({
    @NamedQuery (name = "findAnalysisByProcessId", query = "from ProcessAnalyzed where sTJProcess like ?"),
    @NamedQuery (name = "recAllsTJProcess", query = "select sTJProcess.id from ProcessAnalyzed"),
    @NamedQuery (name = "numberOfProcessAnalyzed", query = "select count(p.id) from "
            + "ProcessAnalyzed p left join p.sTJProcess s "
            + "left join s.query q " 
            + "where q.id = ?"),
    @NamedQuery (name = "valuePerEvent", query = "select new map(p.event, count(p.event),sum(p.value), "
            + "max(p.value), min(p.value), avg(p.value)) from "
            + "ProcessAnalyzed p left join p.sTJProcess s "
            + "left join s.query q "             
            + "where q.id = ?"
            + "group by p.event"),
    @NamedQuery (name = "processAnalyzedByEvents", query = "select p from "
            + "ProcessAnalyzed p left join p.sTJProcess s "
            + "left join s.query q "             
            + "where q.id = ?"
            + "group by p.event "),
    @NamedQuery (name = "paramPerEvent", query = "select new map(pc.text, count(pc.text),sum(p.value), "
            + "max(p.value), min(p.value), avg(p.value)) from "
            + "ProcessAnalyzed p left join p.paramAndCriteria pc "
            + "join p.sTJProcess s left join s.query q "             
            + "where q.id = ? and p.event like ? and pc.text != null "
            + "group by pc.text"),
    @NamedQuery (name = "doctrinePerEvent", query = "select new map(doc.text, count(doc.text),sum(p.value), "
            + "max(p.value), min(p.value), avg(p.value)) from "
            + "ProcessAnalyzed p left join p.doctrine doc "
            + "join p.sTJProcess s left join s.query q "             
            + "where q.id = ? and p.event like ? and doc.text != null "
            + "group by doc.text"),
    @NamedQuery (name = "peculiaritiesPerEvent", query = "select new map(pec.text, count(pec.text),sum(p.value), "
            + "max(p.value), min(p.value), avg(p.value)) from "
            + "ProcessAnalyzed p left join p.textForPeculiaritiesOfConcreteCase pec "
            + "join p.sTJProcess s left join s.query q "             
            + "where q.id = ? and p.event like ? and pec.text != null "
            + "group by pec.text"),
    @NamedQuery (name = "precedntsPerEvent", query = "select new map(prec.text, count(prec.text),sum(p.value), "
            + "max(p.value), min(p.value), avg(p.value)) from "
            + "ProcessAnalyzed p left join p.textForPrecedents prec "
            + "join p.sTJProcess s left join s.query q "             
            + "where q.id = ? and p.event like ? and prec.text != null "
            + "group by prec.text"),
    @NamedQuery (name = "valuationPerEvent", query = "select new map(v.text, count(v.text),sum(p.value), "
            + "max(p.value), min(p.value), avg(p.value)) from "
            + "ProcessAnalyzed p left join p.valuationForConcreteCase v "
            + "join p.sTJProcess s left join s.query q "             
            + "where q.id = ? and p.event like ? and v.text != null "
            + "group by v.text"),
})

@Entity
public class ProcessAnalyzed {
    
    @Id
    @GeneratedValue (strategy = GenerationType.AUTO)
    private int id; 
    
    @Column (columnDefinition = "text")
    private String event;          
    
    private double value;
       
    //@Column (columnDefinition = "text")
    @OneToMany (mappedBy = "processAnalyzed", targetEntity = ParamAndCriteria.class, 
            fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<ParamAndCriteria> paramAndCriteria;        //parametros e criterios
    
    //@Column (columnDefinition = "text")
    @OneToMany (mappedBy = "processAnalyzed", targetEntity = Doctrine.class, 
            fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Doctrine> doctrine;                //doutrina
        
    private boolean peculiaritiesOfConcreteCase; 
    
    //@Column (columnDefinition = "text")
    @OneToMany (mappedBy = "processAnalyzed", targetEntity = PeculiaritiesOfConcreteCase.class, 
            fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<PeculiaritiesOfConcreteCase> textForPeculiaritiesOfConcreteCase;
    
    private boolean havePrecedent; 
    
    //@Column (columnDefinition = "text")
    @OneToMany (mappedBy = "processAnalyzed", targetEntity = Precedents.class, 
            fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Precedents> textForPrecedents;
    
    //@Column (columnDefinition = "text")
    @OneToMany (mappedBy = "processAnalyzed", targetEntity = ValuationForConcreteCase.class, 
            fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<ValuationForConcreteCase> valuationForConcreteCase;
    
    @OneToOne
    private STJProcess sTJProcess;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public List<ParamAndCriteria> getParamAndCriteria() {
        return paramAndCriteria;
    }

    public void setParamAndCriteria(List<ParamAndCriteria> paramAndCriteria) {
        this.paramAndCriteria = paramAndCriteria;
    }

    public List<Doctrine> getDoctrine() {
        return doctrine;
    }

    public void setDoctrine(List<Doctrine> doctrine) {
        this.doctrine = doctrine;
    }

    public boolean isPeculiaritiesOfConcreteCase() {
        return peculiaritiesOfConcreteCase;
    }

    public void setPeculiaritiesOfConcreteCase(boolean peculiaritiesOfConcreteCase) {
        this.peculiaritiesOfConcreteCase = peculiaritiesOfConcreteCase;
    }

    public List<PeculiaritiesOfConcreteCase> getTextForPeculiaritiesOfConcreteCase() {
        return textForPeculiaritiesOfConcreteCase;
    }

    public void setTextForPeculiaritiesOfConcreteCase(List<PeculiaritiesOfConcreteCase> textForPeculiaritiesOfConcreteCase) {
        this.textForPeculiaritiesOfConcreteCase = textForPeculiaritiesOfConcreteCase;
    }

    public boolean isHavePrecedent() {
        return havePrecedent;
    }

    public void setHavePrecedent(boolean havePrecedent) {
        this.havePrecedent = havePrecedent;
    }

    public List<Precedents> getTextForPrecedents() {
        return textForPrecedents;
    }

    public void setTextForPrecedents(List<Precedents> textForPrecedents) {
        this.textForPrecedents = textForPrecedents;
    }

    public List<ValuationForConcreteCase> getValuationForConcreteCase() {
        return valuationForConcreteCase;
    }

    public void setValuationForConcreteCase(List<ValuationForConcreteCase> valuationForConcreteCase) {
        this.valuationForConcreteCase = valuationForConcreteCase;
    }
    
    


    public STJProcess getsTJProcess() {
        return sTJProcess;
    }

    public void setsTJProcess(STJProcess sTJProcess) {
        this.sTJProcess = sTJProcess;
    }
    
    public String toString(){
        return event;
    }
          
}
