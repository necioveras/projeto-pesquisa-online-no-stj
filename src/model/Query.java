/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Date;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

/**
 *
 * @author necio
 */
@NamedQueries ({
        @NamedQuery (name = "getAllQueries", query = "from Query"),
        @NamedQuery (name = "getProcessByQueryId", query = "select r from Query q left join q.results r"
                + " where q.id = ?")
})
@Entity
public class Query {
    
    @Id
    @GeneratedValue (strategy = GenerationType.AUTO)
    private int id;
    
    private String text; 
    private String label; 
    
    private Date date;
    
    @OneToMany (mappedBy = "query", targetEntity = STJProcess.class, 
            fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<STJProcess> results; 
    
    public void addProcess(STJProcess p){
        results.add(p);
    }
    
    public Query(){
        results = new LinkedList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public List<STJProcess> getResults() {
        return results;
    }

    public void setResults(List<STJProcess> results) {
        this.results = results;
    }

    @Override
    public String toString() {
        return label;
    }
    
    

}
